# Access Control Routes (`/api/access`)

#### GET `/api/access/all`
- **Description:** Accessible to all users.
- **Response:** Public content.

#### GET `/api/access/user`
- **Description:** Accessible to authenticated users.
- **Response:** User-specific content.

#### GET `/api/access/employer`
- **Description:** Accessible to users with the "employer" role.
- **Response:** Employer-specific content.

#### GET `/api/access/admin`
- **Description:** Accessible to users with the "admin" role.
- **Response:** Admin-specific content.
