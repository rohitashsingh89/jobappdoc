
# Job Routes (`/api/jobs`)

#### POST `/api/jobs`
- **Description:** Create a new job.
- **Request Body:** Job details (title, description, company, category, location, etc.).
- **Response:** Created job details.

#### GET `/api/jobs`
- **Description:** Get all jobs.
- **Response:** List of all jobs.

#### GET `/api/jobs/:id`
- **Description:** Get a specific job by ID.
- **Parameters:** `id` (string) - Job ID.
- **Response:** Job details.

#### PUT `/api/jobs/:id`
- **Description:** Update a specific job by ID.
- **Parameters:** `id` (string) - Job ID.
- **Request Body:** Updated job details.
- **Response:** Updated job details.

#### DELETE `/api/jobs/:id`
- **Description:** Delete a specific job by ID.
- **Parameters:** `id` (string) - Job ID.
- **Response:** Deletion status.
