
# Authentication Routes (`/api/auth`)

#### POST `/api/auth/register`
- **Description:** Register a new user.
- **Role is optional**
- **Request Body:**
  - `username` (string): User's username.
  - `email` (string): User's email address.
  - `password` (string): User's password.
  - `roles` (array of strings): User roles (e.g., "user", "employer", "admin").
- **Response:** User registration status.

#### POST `/api/auth/login`
- **Description:** Authenticate user and get a JWT token for authorization.
- **Request Body:**
  - `username` (string): User's username.
  - `password` (string): User's password.
- **Response:** JWT token for authenticated user.
- Token saved in session

#### POST `/api/auth/logout`
- **Description:** Sign out the currently authenticated user.
- **Response:** Sign-out status.
