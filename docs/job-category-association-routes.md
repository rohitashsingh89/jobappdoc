
# Job-Category Association Routes (`/api/job-category`)

#### POST `/api/job-category/associate/:jobId/:categoryId`
- **Description:** Associate a job with a category.
- **Parameters:** `jobId` (string) - Job ID, `categoryId` (string) - Category ID.
- **Response:** Association status.

#### DELETE `/api/job-category/disassociate/:jobId/:categoryId`
- **Description:** Disassociate a job from a category.
- **Parameters:** `jobId` (string) - Job ID, `categoryId` (string) - Category ID.
- **Response:** Disassociation status.

### GET `/api/job-category/categories/:jobId`
- **Description:** Get categories associated with a specific job.
- **Parameters:**
- `jobId` (string): ID of the job.
- **Response:** List of categories associated with the specified job.
GET `/api/job-category/jobs/:categoryId`
- **Description:** Get jobs associated with a specific category.
- **Parameters:**
- `categoryId` (string): ID of the category.
- **Response:** List of jobs associated with the specified category.
