
# Category Routes (`/api/category`)

#### POST `/api/category`
- **Description:** Create a new category.
- **Request Body:** Category details (name, description, etc.).
- **Response:** Created category details.

#### GET `/api/category`
- **Description:** Get all categories.
- **Response:** List of all categories.

#### GET `/api/category/:id`
- **Description:** Get a specific category by ID.
- **Parameters:** `id` (string) - Category ID.
- **Response:** Category details.

#### PUT `/api/category/:id`
- **Description:** Update a specific category by ID.
- **Parameters:** `id` (string) - Category ID.
- **Request Body:** Updated category details.
- **Response:** Updated category details.

#### DELETE `/api/category/:id`
- **Description:** Delete a specific category by ID.
- **Parameters:** `id` (string) - Category ID.
- **Response:** Deletion status.
