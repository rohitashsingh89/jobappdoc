document.addEventListener('DOMContentLoaded', function() {
    var externalLinks = document.querySelectorAll('.external-link');
  
    externalLinks.forEach(function(link) {
      link.setAttribute('target', '_blank');
      link.setAttribute('rel', 'noopener noreferrer');
    });
  });
  